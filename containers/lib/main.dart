import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  State createState() => _MainApp();
}

class _MainApp extends State {
  int count = 1;
  bool box1 = false;
  bool box2 = false;
  bool box3 = false;

  void nextQuestion() {
    setState(() {
      count++;
    });
  }

  Scaffold question() {
    if (count == 1) {
      return Scaffold(
        body: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                height: 100,
                width: 100,
                color: Colors.black,
              ),
              Container(
                height: 80,
                width: 80,
                color: Color.fromARGB(255, 53, 53, 53),
              ),
              Container(
                height: 70,
                width: 80,
                color: const Color.fromARGB(255, 84, 84, 84),
              )
            ],
          ),
        ),
      );
    } else if (count == 2) {
      return Scaffold(
        appBar: AppBar(
          title: const Text("Dailyflash"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 200,
                decoration: BoxDecoration(border: Border.all(width: 1)),
                child: const Padding(
                  padding: EdgeInsets.all(8),
                  child: Row(
                    children: [
                      Icon(
                        Icons.star_border_rounded,
                        size: 40,
                        color: Colors.orange,
                      ),
                      Text(
                        "Ratings 4.5",
                        style: TextStyle(
                            fontSize: 25, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      );
    } else if (count == 3) {
      return Scaffold(
        body: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                    color: Colors.red,
                    border: Border.all(width: 1),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(20),
                        bottomRight: Radius.circular(20)),
                    boxShadow: const [
                      BoxShadow(
                          offset: Offset(-1, 1),
                          color: Colors.amber,
                          blurRadius: 15)
                    ]),
              ),
              Container(
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                    color: Colors.green,
                    border: Border.all(width: 1),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(20),
                        bottomRight: Radius.circular(20)),
                    boxShadow: const [
                      BoxShadow(
                          offset: Offset(-1, 1),
                          color: Colors.deepPurpleAccent,
                          blurRadius: 15)
                    ]),
              )
            ],
          ),
        ),
      );
    } else if (count == 4) {
      return Scaffold(
        body: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                height: 100,
                width: 300,
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                  Colors.red,
                  Colors.blue,
                  Colors.green
                ], stops: [
                  0.2,
                  0.6,
                  0.0,
                ])),
              )
            ],
          ),
        ),
      );
    } else {
      return Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    height: 100,
                    width: 300,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(colors: [
                      Colors.red,
                      Colors.blue,
                      Colors.green
                    ], stops: [
                      0.2,
                      0.6,
                      0.0,
                    ])),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    height: 100,
                    width: 300,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(colors: [
                      Colors.red,
                      Colors.blue,
                      Colors.green
                    ], stops: [
                      0.2,
                      0.6,
                      0.0,
                    ])),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    height: 100,
                    width: 300,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(colors: [
                      Colors.red,
                      Colors.blue,
                      Colors.green
                    ], stops: [
                      0.2,
                      0.6,
                      0.0,
                    ])),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      body: question(),
      floatingActionButton: FloatingActionButton(
        onPressed: nextQuestion,
        child: const Text("Next"),
      ),
    ));
  }
}
